<?php
  session_start();

  if(!isset($_SESSION['isLoggdeIn'])){
    header('Location: login.php');
    exit();
  }
?>

<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
  <title>G-stock</title>
  <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Aldrich">
  <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
  <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
  <link rel="stylesheet" href="assets/fonts/fontawesome5-overrides.min.css">
  <link rel="stylesheet" href="assets/css/dashboard-header-navigation.css">
  <link rel="stylesheet" href="assets/css/Footer-Clean.css">
  <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
  <link rel="stylesheet" href="assets/css/Login-Red.css">
  <link rel="stylesheet" href="assets/css/Sidebar-1.css">
  <link rel="stylesheet" href="assets/css/Sidebar-Menu-1.css">
  <link rel="stylesheet" href="assets/css/Sidebar-Menu.css">
  <link rel="stylesheet" href="assets/css/Sidebar.css">
  <link rel="stylesheet" href="assets/css/Simple-Header-y-Navbar-adaptativo-1.css">
  <link rel="stylesheet" href="assets/css/Simple-Header-y-Navbar-adaptativo.css">
  <link rel="stylesheet" href="assets/css/styles.css">

  <style>
    div.content {
      padding: 1px 16px;
    }
  </style>
</head>

<body>
  <!DOCTYPE html>
  <html lang="en">

  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!--<link rel="stylesheet" href="css/main.css">-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  </head>

  <body>
    <header>
      <h1>Gestion de stock</h1>
      <div align="right"><a href="controller/process/logout_process.php" class="fa fa-sign-out" aria-hidden="true"></a></div>
    </header>
    <nav>
      <ul>
        <a href="">
          <li><i class="fa fa-home"></i> Accueil</li>
        </a>
        <a href="">
          <li><i class="fa fa-info"></i> Articles</li>
        </a>
        <a href="">
          <li><i class="fa fa-inbox"></i> Commande</li>
        </a>
      </ul>
      <div class="handle">
        Menu
      </div>
    </nav>

    <script>
      $('.handle').on('click', function () {
        $('nav ul').toggleClass('showing');
      });

      $(document).ready(function () {
        //Disable return back browser***************************/
        function noBack(){window.history.forward()}
        noBack();
        window.onload=noBack;
        window.onpageshow=function(evt){if(evt.persisted)noBack()}
        window.onunload=function(){void(0)}
        /********************************************************/

        $('a[href="#items"]').click(function () {
          //alert("Items");
          request_post("items");
        });

        $('a[href="#commands"]').click(function () {
          //alert("Commands");
          request_post("commands");
        });
      });

      function request_post(id_title) {
        $.post("controller/controller.php", { id: id_title }, function (r) {
          $("#content").html(r);
        });
      }

    </script>
  </body>

  </html>
  <div id="wrapper">
    <div id="sidebar-wrapper">
      <ul class="sidebar-nav">
        <li class="sidebar-brand"><a class="nav-link active" href="#items">Articles</a></li>
        <li><a class="nav-link" href="#commands">Commandes</a></li>
      </ul>
    </div>
    <div class="" style=""><a class="btn btn-link" role="button" id="menu-toggle" href="#menu-toggle"><i class="fa fa-bars"></i></a>
      <div class="content" align="center" style="margin-top:-40px" id="content">

      </div>
    </div>
    


  </div>
  <script src="assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="assets/js/Sidebar-Menu.js"></script>
</body>

</html>