<?php
    class dbObj{        
        private $objConn;       

        function __construct(){
            define('DSN', 'mysql:host=localhost;dbname=g_stock_new');
            define('DB_USER', 'root');
            define('DB_PASSWORD', '');

            try {
                $this->objConn = new PDO(DSN, DB_USER, DB_PASSWORD);
                //echo "Connexion BD Ok !";
                $this->objConn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (PDOException $e) {
                $errorMessage = $e->getMessage();
                echo "Erreur lors de la connexion à la BD : ".$errorMessage;
                exit();
            }
        }

        public function request($sql){
            $r = $this->objConn->query($sql);
            if(!$r)
                echo "Erreur requete SQL !!!";
            else
                return $r;
        }


    }
    

?>