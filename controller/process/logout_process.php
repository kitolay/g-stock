<?php
    session_start();

    unset($_SESSION['isLoggdeIn']);
    session_destroy();
    header('Location: ../../login.php');
?>