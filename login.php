<?php
    session_start();

    if(isset($_SESSION['isLoggdeIn'])){
        header('Location: index.php');
        exit();
    }
        
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>G-stock</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/bootstrap/css/cerulean.theme.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Aldrich">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="assets/css/dashboard-header-navigation.css">
    <link rel="stylesheet" href="assets/css/Footer-Clean.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Login-Red.css">
    <link rel="stylesheet" href="assets/css/Sidebar-1.css">
    <link rel="stylesheet" href="assets/css/Sidebar-Menu-1.css">
    <link rel="stylesheet" href="assets/css/Sidebar-Menu.css">
    <link rel="stylesheet" href="assets/css/Sidebar.css">
    <link rel="stylesheet" href="assets/css/Simple-Header-y-Navbar-adaptativo-1.css">
    <link rel="stylesheet" href="assets/css/Simple-Header-y-Navbar-adaptativo.css">
    <link rel="stylesheet" href="assets/css/styles.css">

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>

    <style>
        .field-icon {
            float: right;
            margin-left: -25px;
            margin-top: -25px;
            margin-right: 5px;
            position: relative;
            z-index: 2;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function(){
             //Disable return back browser***************************/
            function noBack(){window.history.forward()}
            noBack();
            window.onload=noBack;
            window.onpageshow=function(evt){if(evt.persisted)noBack()}
            window.onunload=function(){void(0)}
            /********************************************************/

            //Show/Hide password
            $(".toggle-password").click(function() {
                
                $(this).toggleClass("fa-eye fa-eye-slash");
                var input = $($(this).attr("toggle"));
                if (input.attr("type") == "password") {
                    input.attr("type", "text");
                } else {
                    input.attr("type", "password");
                }
                
            });
            /********************************************************/

            $("#btn_login").click(function(){
                let login = $("#login").val();
                let password = $("#password").val();                

                if(login == "" || password == ""){
                    $("#login_failed").css({"display":"none"});
                    $("#login_error").css({"display":"block"});
                }                   
                else
                    $("#login_error").css({"display":"none"});

                if(login != "" && password != ""){
                    $.post("controller/process/login_process.php", {login: login, password: password}, function(r){                     
                        if(r == 0){
                            $("#login_error").css({"display":"none"});
                            $("#login_failed").css({"display":"block"});
                        }else{
                            $("#login_success").css({"display":"block"});
                            window.location.href="index.php";
                        }
                            
                    });
                }
                
            });
        });
  
    </script>
</head>


<body style="min-height: 800px;max-height: 800px;">

    <div class="container-fluid">
        <div class="row mh-100vh" style="background-image:url(183713.jpg);background-size:cover;">
            <div class="col-12 col-sm-8 col-md-6 col-lg-6 offset-sm-2 offset-md-3 offset-lg-3 align-self-center d-lg-flex align-items-lg-center align-self-lg-stretch bg-white p-5 rounded rounded-lg-0 my-5 my-lg-0 my-box-shadow"
                style="/*background-color: transparent !important;*/">
                <div class="m-auto w-lg-75 w-xl-50" style="padding: 47px;border: 1px solid gray;">
                    <div class="row loginValidation">
                        <div id="login_error" style="font-size:10px;display:none" class="alert alert-danger"><!--<button type="button" class="close" data-dismiss="alert">&times;</button>-->
                            <span style="" class="fa fa-info-circle" aria-hidden="true"></span>&nbsp;&nbsp;Tous les champs sont requis 
                        </div>
                        <div id="login_success" style="font-size:11px;display:none" class="alert alert-success">Connexion réussie ...</div>
                        <div id="login_failed" style="font-size:10px;display:none" class="alert alert-danger"><!--<button type="button" class="close" data-dismiss="alert">&times;</button>-->
                            <span style="" class="fa fa-info-circle" aria-hidden="true"></span>&nbsp;&nbsp;Login ou mot de passe incorrect 
                        </div>
                    </div>                  
                    <div class="row">
                        <h1 align="center" style="color:#B20202">G-stock</h1>
                    </div>
                    <div class="row justify-content-center" style="margin-bottom: 37px;">
                        <div class="col-auto align-items-center align-content-center" style="/*text-align: center;*/"><i class="fa fa-fire" style="/*text-align: center;*/font-size: 87px;color: #b81717;"></i>
                        </div>
                    </div>
                    <form>
                        <div class="form-group mb-3">
                            <label class="form-label text-secondary" style="color: #b20202 !important;">Login</label>
                            <input id="login" class="form-control" type="text" required />
                            <!--<input id="login" class="form-control" type="text" required pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,15}$" inputmode="email" />-->
                        </div>
                        <div class="form-group mb-3">
                            <label class="form-label text-secondary" style="color: #b20202 !important;">Mot de passe</label>
                            <input id="password" class="form-control" type="password" required />
                            <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                        </div>
                        <div class="row justify-content-end">
                            <div class="col-auto align-self-center" style="text-align: right;">
                                <button id="btn_login" class="btn btn-danger d-flex outline" style="border-radius: 20px;" type="button">Se connecter</button>
                            </div>
                        </div>
                    </form>
                    <p class="text-end mt-3 mb-0"><a class="link-danger text-info small" href="#">Mot de passe oublié ?</a></p>                    
                </div>
            </div>
        </div>
    </div>
    <!--<script src="assets/bootstrap/js/bootstrap.min.js"></script>-->
    <!--<script src="assets/js/Sidebar-Menu.js"></script>-->
</body>

</html>